#l10n_firefox
Firefox extension for Localization Glossary Tooltips

Data
====
The terms source is https://traduction.drupal.fr/glossaire.json

Dev
===
#Install web-ext
sudo npm install --global web-ext
#Go to extension folder
cd l10n_firefox/
#Lunch with a custom profile (here named ffmylanguagetests
web-ext run --keep-profile-changes --firefox-profile=ffmylanguagetests --url https://localize.drupal.org/translate/languages/fr/translate --bc
#Build
web-ext build