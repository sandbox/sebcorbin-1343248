// ==UserScript==
// @name           L10n glossary
// @namespace      l10n_glossary
// @description    Met en évidence les termes du glossaire sur localize
// @author         http://sebcorbin.fr/
// @homepage       http://traduction.drupalfr.org/
// @include        htt*://localize.drupal.org/translate/languages/fr/translate*
// @require		   jquery.js
// @require		   tooltip.js
// @icon           http://traduction.drupalfr.org/sites/default/files/dfr-logo-la-rache_66x73.png
// @version        1.0
// ==/UserScript==

()
setInterval('alert(jQuery);', 1000);
GM_log('ok');

/*
jQuery.fn.addSpan = function (str, title)
{
	return this.each(function () {
		var rgxp = new RegExp(str, 'g');
		var repl = '<span title="' + title.replace(/\"/g,"\\\"") + '">' + str + '</span>';
		this.innerHTML = this.innerHTML.replace(rgxp, repl);
	});
};

function updateTerms(thenParse) {
	// Update glossary terms every hour
	glossaryLast = GM_getValue("glossaryLast", false);
	if(!glossaryLast || !GM_getValue("glossaryTerms", false) || glossaryLast < Date.now() - 1000*60*60) {
		// Download glossary items
		GM_log("Downloading glossary terms");
		GM_xmlhttpRequest({
			method: 'GET',
			url: 'http://traduction.drupalfr.org/glossaire.json',
			onreadystatechange: function(response) {
				if (response.readyState == 4) {
					if (response.status == 200) {
						var data = JSON.parse(response.responseText);
						// Mise en forme des termes récupérés
						var terms = {};
						for(var t in data.terms) {
							terms[sanitize(data.terms[t].term.title)] = data.terms[t].term.field_traduction;
						}
						GM_setValue("glossaryTerms", JSON.stringify(terms));
						GM_setValue("glossaryLast", Date.now());
						GM_log("Termes du glossaire mis à jour");
						if(thenParse) {
							parseEnglishStrings(terms);
						}
					}
				}
			}
		});
	}
}
setInterval('updateTerms(false);', 1000 * 60 * 5);

var terms;
if(terms = GM_getValue("glossaryTerms", false)) {
	terms = JSON.parse(terms);
	parseEnglishStrings(terms);
}
else {
	updateTerms(true);
}

/**
 * Sanitizing function
 */
function sanitize(term) {
	if(term.indexOf(' (to)') > -1) {
		term = term.substr(0, term.indexOf(' (to)'));
	}
	return term.toLowerCase();
}

/**
 * Parsing function
 */
function parseEnglishStrings(terms) {
	jQuery('.source .l10n-string span').each(function() {
		for(var term in terms) {
			// d'abord un indexOf pour la condition
			if(jQuery(this).html().indexOf(term) > - 1) {
				// puis une regex pour la boucle
				GM_log("Terme trouvé : "+term);
				jQuery(this).addSpan(term, terms[term]);
			}
		}
	});
	jQuery('.source .l10n-string > span > span[title]').css('background-color', '#ff0').css('cursor', 'pointer');
	GM_log(jQuery('.source .l10n-string > span > span[title]').tooltip());
}
setInterval("jQuery('.source .l10n-string > span > span[title]')", 1000 * 3);


/**
 * Tooltip styling. by default the element to be styled is .tooltip  
 */
var style = ".tooltip {"+
"	display:none;"+
"	background:transparent url(http://traduction.drupalfr.org/sites/default/files/tooltip.png) no-repeat !important;"+
"	font-size:12px !important;"+
"	height:55px !important;"+
"	width:114px !important;"+
"	padding:2px 2px 0 2px !important;"+
"	color:#fff;"+
"	z-index: 200;"+
"	text-align: center;"+
"}";
GM_addStyle(style);
