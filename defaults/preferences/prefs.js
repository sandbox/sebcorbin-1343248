pref("extensions.l10n_firefox.boolpref", false);
pref("extensions.l10n_firefox.intpref", 0);
pref("extensions.l10n_firefox.stringpref", "A string");

// https://developer.mozilla.org/en/Localizing_extension_descriptions
pref("extensions.l10n_firefox.description", "chrome://l10n_firefox/locale/overlay.properties");
