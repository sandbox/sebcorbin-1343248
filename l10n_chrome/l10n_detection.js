/**
 * Used to detect if there's something to do with the glossary
 */
// On localize.drupal.org or l10n_client enabled pages
if ((location.host == "localize.drupal.org" && location.pathname == "/translate/languages/fr/translate") || document.getElementById('l10n-client')) {
  chrome.runtime.sendMessage({'action' : 'insertScripts'});
}
