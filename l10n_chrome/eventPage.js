/**
 * Checks the notifications behavior
 *
 * DEPRECATED: http://stackoverflow.com/questions/23873149/window-webkitnotifications-checkpermission-not-working
 */
/* if (window.webkitNotifications.checkPermission() != 0) {
  window.webkitNotifications.requestPermission();
} */

/**
 * Updates terms and stores them in localStorage
 */
function updateTerms(force) {
  // Update glossary terms every hour
  glossaryLast = localStorage["glossaryLast"];
  if (force || !glossaryLast || !localStorage["glossaryTerms"] || glossaryLast < Date.now() - 1000 * 60 * 60) {
    // Download glossary items
    /*
     var notification = window.webkitNotifications.createNotification(
     'icon-48.png', // The image.
     "Téléchargement des termes", // The title.
     'Les termes du glossaire sont en cours de mise à jour.'      // The body.
     );
     notification.show();
     */
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function(data) {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          var data = JSON.parse(xhr.responseText);
          var terms = processJsonTerms(data.terms);
          localStorage["glossaryTerms"] = JSON.stringify(terms);
          localStorage["glossaryLast"] = Date.now();
          //notification.cancel();
        }
      }
    }
    // Note that any URL fetched here must be matched by a permission in
    // the manifest.json file!
    var url = 'http://traduction.drupalfr.org/glossaire.json';
    xhr.open('GET', url, true);
    xhr.send();
  }
}
// Try to update every ten seconds
setInterval(updateTerms, 10000);

/**
 * Sanitizing function
 */
function sanitize(term) {
  if (term.indexOf(' (to)') > -1) {
    term = term.substr(0, term.indexOf(' (to)'));
  }
  return term.toLowerCase();
}

/**
 * Performs an XMLHttpRequest to Twitter's API to get trending topics.
 * @param callback Function If the response from fetching url has a
 *    HTTP status of 200, this function is called with a JSON decoded
 *    response.  Otherwise, this function is called with null.
 */
function fetchTerms(callback) {
  var terms = JSON.parse(localStorage["glossaryTerms"]);
  callback(terms);
}

/**
 * Handles data sent via chrome.extension.sendMessage().
 *
 * @param request Object Data sent in the request.
 * @param sender Object Origin of the request.
 * @param callback Function The method to call when the request completes.
 */
function onMessage(request, sender, callback) {
  if (request.action == 'fetchTerms') {
    fetchTerms(callback);
  }
  if (request.action == 'insertScripts') {
    insertScripts(request, sender);
  }
}

/**
 * Mise en forme des termes récupérés
 *
 * @param data array of terms
 */
function processJsonTerms(data) {
  var terms = {};
  for (var t in data) {
    // Check if the form is already in the array
    if (terms[sanitize(data[t].term.title)]) {
      var isVerb = data[t].term.title.indexOf(' (to)') > -1;
      var verb = isVerb ? data[t].term.field_traduction : terms[sanitize(data[t].term.title)];
      var other = !isVerb ? data[t].term.field_traduction : terms[sanitize(data[t].term.title)];
      terms[sanitize(data[t].term.title)] = "<ul><li><strong>Verbe :</strong> " + verb +
              "</li><li><strong>Autre :</strong> " + other + "</li></ul>";
    }
    else {
      terms[sanitize(data[t].term.title)] = data[t].term.field_traduction;
    }
  }
  return terms;
}

/**
 * Insert the scripts in the current tab
 */
function insertScripts(a, b) {
  chrome.tabs.insertCSS(null, {file:"style.css"})
  chrome.tabs.executeScript(null, {file:"jquery.js"});
  chrome.tabs.executeScript(null, {file:"jquery.tipsy.js"});
  chrome.tabs.executeScript(null, {file:"script.js"});
}

// Wire up the listener.
chrome.runtime.onMessage.addListener(onMessage);
