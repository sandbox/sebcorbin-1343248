/**
 * Used to detect if there's something to do with the glossary
 */
// On localize.drupal.org or l10n_client enabled pages
if ((location.host == "localize.drupal.org" && location.pathname == "/translate/languages/fr/translate") || document.getElementById('l10n-client')) {
  // Send a request to fetch terms from localStorage.
  // Specify that parsing function should be called with the result.
  safari.self.tab.dispatchMessage("fetchTerms");
  safari.self.addEventListener("message", getTerms, false);
}

function getTerms(event) {
  if (event.name === "getTerms") {
    parseEnglishStrings(event.message);
  }
}

/**
 * Callback function
 * @param terms Glossary terms
 */
function parseEnglishStrings(terms) {
  // Bind the event for l10n_client
  $('#l10n-client-string-select li').click(function() {
    $('#l10n-client-string-editor .source-text').each(function() {
      processEnglishStrings(terms, this);
    });
  });

  // Directly parse on localize
  $('.source .l10n-string span').each(function() {
    processEnglishStrings(terms, this);
  });
}

/**
 * Actual processing function
 * @param terms Glossary terms
 * @param elem Element where to highlight terms
 */
function processEnglishStrings(terms, elem) {
  var text = $(elem).html();

  var bits = [text];
  // First things first, clear out l10n_server parts
  if ($('span, em, code', elem).length) {
    var regExp = /<([A-Z][A-Z0-9]*)\b.*?>.*?<\/\1>/i;
    var found, b = 0;
    // While there are tags
    while (found = regExp.exec(bits[bits.length - 1])) {
      // Split the array in parts marking the l10n HTML as processed
      bits.splice(b, 1,
              bits[b].substr(0, bits[b].indexOf(found[0])),
              {processed: true, original: found[0]},
              bits[b].substr(bits[b].indexOf(found[0]) + found[0].length)
      );
      b += 2;
    }
  }

  // So now we have an array of parts containing either string or already processed objects
  // Let's try to do a replacement on those strings
  var refresh = false;
  do {
    refresh = false;
    toRefresh : for (var b in bits) {
      // Process the bit if it has not and if it's not an empty string
      if (!bits[b].processed && bits[b].length) {
        var f;
        for (term in terms) {
          if (f = bits[b].match(new RegExp('(' + term.replace(/\./g, "\\.") + ')', 'ig'))) {
            bits.splice(b, 1,
                    // First element is a clean string or an empty one
                    bits[b].indexOf(f[0]) > 0 ? bits[b].substr(0, bits[b].indexOf(f[0])) : '',
                    // Second element is a processed object to be tipped
                    {processed: true, original: f[0], toBeReplacedWith: terms[term]},
                    // Third element is also a clean string
                    bits[b].substr(bits[b].indexOf(f[0]) + f[0].length)
            );
            // We have found something, mark this as to be done again from the beginning
            refresh = true;
            break toRefresh;
          }
        }
      }
    }
  }
    // Repeat that until we do not find any term
  while (refresh);

  // Now we add our little tip on each term that have been porcessed
  for (var b in bits) {
    if (bits[b].processed) {
      if (bits[b].toBeReplacedWith) {
        // Adding the tipsy span on the terms
        bits[b] = '<span t="' + bits[b].toBeReplacedWith + '">' + bits[b].original + '</span>';
      }
      else {
        // Replacing with the original for the former l10n HTML
        bits[b] = bits[b].original;
      }
    }
  }

  // Bring the parts together to form a string
  elem.innerHTML = bits.join("");

  var span = $('span[t]', elem);

  // Style stuff
  span.css('background-color', '#ff0').css('cursor', 'pointer');
  span.tipsy({html: true, fade: true, delayOut: 1000, title: 't', gravity: 's'});

  // Event stuff
  span.click(function() {
    var newTranslation = $('.new-translation textarea', $(elem).parents('tr'));
    if (newTranslation.val() == "<New translation>") {
      newTranslation.val($(elem).attr('t')).keyup();
    }
    else {
      newTranslation.val(newTranslation.val() + $(elem).attr('t')).keyup();
    }
    newTranslation.focus();
  });
}
